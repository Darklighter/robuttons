#include "mainwindow.h"

#include <QStatusBar>

MainWindow::MainWindow() {
	tableWidthSpinBox.setValue(simulationWidget.tableWidth);
	tableWidthSpinBox.setRange(6,1000);
	QObject::connect(&tableWidthSpinBox, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), &simulationWidget, static_cast<void(SimulationWidget::*)(int)>(&SimulationWidget::updateTableWidth));
	QObject::connect(&tableWidthSpinBox, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &MainWindow::resetRobuttonCount);

	tableHeightSpinBox.setValue(simulationWidget.tableHeight);
	tableHeightSpinBox.setRange(6,1000);
	QObject::connect(&tableHeightSpinBox, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), &simulationWidget, static_cast<void(SimulationWidget::*)(int)>(&SimulationWidget::updateTableHeight));
	QObject::connect(&tableHeightSpinBox, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &MainWindow::resetRobuttonCount);

	resetRobuttonCount();//range for robuttons and coins
	robuttonCountSpinBox.setValue(150);
	coinCountSpinBox.setValue(200);

	stepSpinBox.setRange(0.01, 0.5);
	stepSpinBox.setSingleStep(0.01);
	stepSpinBox.setValue(simulationWidget.getStepValue());
	QObject::connect(&stepSpinBox, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), &simulationWidget, &SimulationWidget::setStepValue);

	timeSpinBox.setRange(0, 5000);
	timeSpinBox.setValue(simulationWidget.simTimerTime());
	QObject::connect(&timeSpinBox, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), &simulationWidget, static_cast<void(SimulationWidget::*)(int)>(&SimulationWidget::simTimerTime));

	syncSpinBox.setRange(0, 5000);
	syncSpinBox.setValue(simulationWidget.getDisplayTimerTime());
	QObject::connect(&syncSpinBox, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), &simulationWidget, static_cast<void(SimulationWidget::*)(int)>(&SimulationWidget::setDisplayTimerTime));

	cornerSpinBox.setRange(3, 10000);
	cornerSpinBox.setValue(simulationWidget.corner());
	QObject::connect(&cornerSpinBox, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), &simulationWidget, static_cast<void(SimulationWidget::*)(int)>(&SimulationWidget::corner));


	startStopButton.setMinimumHeight(40);
	QObject::connect(&startStopButton, &QPushButton::clicked, this, &MainWindow::startStop);
	pauseButton.setMaximumWidth(100);
	pauseButton.setMinimumHeight(40);
	pauseButton.setEnabled(false);
	QObject::connect(&pauseButton, &QPushButton::clicked, this, &MainWindow::pauseUnpause);


	check1.setChecked(simulationWidget.alterCollisionRule());
	check1.setMaximumWidth(100);
	QObject::connect(&check1, &QCheckBox::toggled, &simulationWidget, static_cast<void(SimulationWidget::*)(bool)>(&SimulationWidget::alterCollisionRule));

	comboSector.setMaximumWidth(100);
	comboSector.addItem("no Sectors");
	comboSector.addItem("Sector 1x1");
	comboSector.addItem("Sector 2x2");
	comboSector.addItem("Sector 3x3");
	comboSector.setCurrentIndex(simulationWidget.getSectorChoice());
	QObject::connect(&comboSector, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), &simulationWidget, static_cast<void(SimulationWidget::*)(int)>(&SimulationWidget::requestSectorChoice));

	combo1.setMaximumWidth(100);
	combo1.addItem("no AA");
	combo1.addItem("Blend");
	combo1.addItem("MSAA");
	combo1.setCurrentIndex(simulationWidget.antiAlias());
	QObject::connect(&combo1, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), &simulationWidget, static_cast<void(SimulationWidget::*)(int)>(&SimulationWidget::antiAlias));

	check2.setChecked(simulationWidget.shadowValue());
	check2.setMaximumWidth(100);
	QObject::connect(&check2, static_cast<void(QCheckBox::*)(bool)>(&QCheckBox::toggled), &simulationWidget, static_cast<void(SimulationWidget::*)(bool)>(&SimulationWidget::shadowValue));

	check3.setChecked(simulationWidget.outlines());
	check3.setMaximumWidth(100);
	QObject::connect(&check3, static_cast<void(QCheckBox::*)(bool)>(&QCheckBox::toggled), &simulationWidget, static_cast<void(SimulationWidget::*)(bool)>(&SimulationWidget::outlines));


	connect(&simulationWidget, SIGNAL(showMessage(QString)), statusBar(), SLOT(showMessage(QString)));
//	QObject::connect(&simulationWidget, &SimulationWidget::showMessage, [&](const QString & str){emit mainwindow.statusBar()->showMessage(str);});//ultra buggy

	int col = 0;
	controlLayout.addWidget(&tableWidthLabel, 0, col++);
	controlLayout.addWidget(&tableHeightLabel, 0, col++);
	controlLayout.addWidget(&robuttonCountLabel, 0, col++);
	controlLayout.addWidget(&coinCountLabel, 0, col++);
	controlLayout.addWidget(&stepLabel, 0, col++);
	controlLayout.addWidget(&timeLabel, 0, col++);
	controlLayout.addWidget(&syncLabel, 0, col++);
	controlLayout.addWidget(&cornerLabel, 0, col++);
	col = 0;
	controlLayout.addWidget(&tableWidthSpinBox, 1, col++);
	controlLayout.addWidget(&tableHeightSpinBox, 1, col++);
	controlLayout.addWidget(&robuttonCountSpinBox, 1, col++);
	controlLayout.addWidget(&coinCountSpinBox, 1, col++);
	controlLayout.addWidget(&stepSpinBox, 1, col++);
	controlLayout.addWidget(&timeSpinBox, 1, col++);
	controlLayout.addWidget(&syncSpinBox, 1, col++);
	controlLayout.addWidget(&cornerSpinBox, 1, col++);
	startPauseLayout.addWidget(&startStopButton);
	startPauseLayout.addWidget(&pauseButton);
	startPauseLayout.addWidget(&check1);
	startPauseLayout.addWidget(&comboSector);
	startPauseLayout.addWidget(&combo1);
	startPauseLayout.addWidget(&check2);
	startPauseLayout.addWidget(&check3);

	layout.addWidget(&simulationWidget);
	layout.addLayout(&controlLayout);
	layout.addLayout(&startPauseLayout);
	centralWidget.setLayout(&layout);
	setCentralWidget(&centralWidget);

	resize(700, 600);//default size
	showMaximized();
}

void MainWindow::resetRobuttonCount() {
	robuttonCountSpinBox.setRange(1, ((tableHeightSpinBox.value()/2) * (tableWidthSpinBox.value()/2))/4);
	coinCountSpinBox.setRange(0, ((tableHeightSpinBox.value()/2) * (tableWidthSpinBox.value()/2))/3);
}

void MainWindow::startStop() {
	if (isRunning) {
		simulationWidget.stop();
		enableControls(true);
	} else {
		enableControls(false);
		simulationWidget.start(coinCountSpinBox.value(),robuttonCountSpinBox.value());
	}
	isRunning = !isRunning;
}

void MainWindow::pauseUnpause() {
	if (isPaused) {
		simulationWidget.resume();
		pauseButton.setText("Pause");
	} else {
		simulationWidget.pause();
		pauseButton.setText("Continue");
	}
	isPaused = !isPaused;
}

void MainWindow::enableControls(bool flag) {
	tableWidthLabel.setEnabled(flag);
	tableHeightLabel.setEnabled(flag);
	robuttonCountLabel.setEnabled(flag);
	coinCountLabel.setEnabled(flag);
	tableWidthSpinBox.setEnabled(flag);
	tableHeightSpinBox.setEnabled(flag);
	coinCountSpinBox.setEnabled(flag);
	robuttonCountSpinBox.setEnabled(flag);

	if (isPaused) {
		pauseUnpause();
	}
	pauseButton.setEnabled(!flag);

	if (flag) {
		startStopButton.setText("Start simulation");
	} else {
		startStopButton.setText("Stop simulation");
	}

	repaint();
}
