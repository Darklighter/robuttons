#include "SimulationWidget.h"
#include "Tfred.h"

using namespace Robuttons;

//Konstruktor, setzen der globalen Variablen auf die Standartwerte
SimulationWidget::SimulationWidget(QWidget *parent)
	: QGLWidget(parent)
{
	setMinimumSize(100, 100);
	running=false;
	tableWidth=76;
	tableHeight=33;
	coinCount=0;
	robuttonCount=0;

	sectors = NULL;
	unused = NULL;
	aa = 1;	
	shdw = false;
	outline = true;
	alterCollisionRules = true;
	sectorChoice = 1;
	sectorSize = 1;
	simTime = 20;
	displayTime = 20;
	corners = 20;
	step = 0.10;
	setAutoBufferSwap(false);
	calcer = new Tfred(this, Tfred::WorkHints::calcer);
	displayer = new Tfred(this, Tfred::WorkHints::displayer);
	informer = new Tfred(this, Tfred::WorkHints::informer);
	context()->moveToThread(displayer);//Qt5

	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}
SimulationWidget::~SimulationWidget() {
	calcer->pause();
	displayer->pause();
	while(calcer->working() || displayer->working()) Sleep(1);
}
void SimulationWidget::resizeEvent(QResizeEvent* evt) {
	resize();
	evt->accept();
}
void SimulationWidget::paintEvent(QPaintEvent* evt) {
	displayer->immediateUpdate();
	evt->accept();
}
Coin *private_coins = NULL;
Robutton *private_robuttons = NULL;
float zoom = 1;
void SimulationWidget::drawTest()  {
	QTime t;
	t.start();

	glEnable(GL_BLEND);

	glClear(GL_DEPTH_BUFFER_BIT);// lösche Tiefen-Informationen
	glClearColor(0.95f, 0.95f, 0.95f, 0);// setze Lösch-Farbe
	glClear(GL_COLOR_BUFFER_BIT);// lösche Hintergrund
	
	private_coins = coins; private_robuttons = robuttons;

	glLoadIdentity();
	glColor3f(0,0,0);// schwarzer Rahmen
	glBegin(GL_QUADS);
		glVertex2f(0/scale, 0/scale);
		glVertex2f(0/scale, tableHeight);
		glVertex2f(tableWidth, tableHeight);
		glVertex2f(tableWidth, 0/scale);
	glEnd();
	glColor3f(1,1,1);// weißer Tisch
	glBegin(GL_QUADS);
		glVertex2f(1/scale, 1/scale);
		glVertex2f(1/scale, tableHeight-1/scale);
		glVertex2f(tableWidth-1/scale, tableHeight-1/scale);
		glVertex2f(tableWidth-1/scale, 1/scale);
	glEnd();

	for(int i=0; i<coinCount; i++) {
		glTranslatef(private_coins[i].x, private_coins[i].y, 0);
		glColor3f(0.0, 0.0, 0.0); // schwarz
		glCallList(1);
		glColor3f(0.9f, 0.9f, 0.0); // gelb
		glCallList(2);
		glLoadIdentity();
	}
	//70ms
	//135ms
	//Robuttons
	glColor3f(0.0, 0.0, 0.0); // schwarz
	for(int i=0; i<robuttonCount; i++) {
		glTranslatef(private_robuttons[i].x, private_robuttons[i].y, 0);
		glCallList(1);
		glLoadIdentity();
	}
	//193ms
	for(int i=0; i<robuttonCount; i++) {
		if(private_robuttons[i].carries) glColor3f(0.9f, 0.0, 0.0); // rot
		else if(private_robuttons[i].blocked) glColor3f(0.9f, 0.9f, 0.9f); // weiß
		else glColor3f(0.0, 0.9f, 0.0); // grün
		glTranslatef(private_robuttons[i].x, private_robuttons[i].y, 0);
		glCallList(2);
		glLoadIdentity();
	}
	//241ms
	//Augen
	glColor3f(0.0, 0.0, 0.0);
	glPointSize(0.2*scale);
	glBegin(GL_POINTS);
	for(int i=0; i<robuttonCount; i++)
		glVertex2f(private_robuttons[i].x+0.7*sin(private_robuttons[i].angle * 3.14159265 / 180.0 ),
		private_robuttons[i].y+0.7*cos(private_robuttons[i].angle * 3.14159265 / 180.0 ));
	glEnd();

	times[8] = t.elapsed();

	swapBuffers();

	times[1]++; //draw call counter
	times[2] = t.elapsed();
}
void SimulationWidget::draw()  {
	QTime t;
	t.start();
	
	//glFinish();// returns after a complete pipeline flush

	//zoom += 0.001f;
	//glLoadIdentity();
	//glScalef(zoom, zoom, 1);
	//glTranslatef(0.5f*tableWidth*(1-zoom), 0.5f*tableHeight*(1-zoom), 0); //mittig halten
	//scale*=zoom;

	/*
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-movex/scale-tableWidth*(1-zoom)
		, tableWidth+2*tableWidth*(1-zoom)+movex/scale
		, tableHeight+movey/scale
		, -movey/scale, -1.0, 1.0);
	//glOrtho(-movex/scale, tableWidth+movex/scale, tableHeight+movey/scale, -movey/scale, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	*/
	//glScalef(0.99f, 0.99f, 1);
	//glTranslatef(0.5f*tableWidth*(1-0.99f), 0.5f*tableHeight*(1-0.99f), 0);
	//glRotatef(5, 0, 0, 0);

	switch (aa)
	{
	case 0: glDisable(GL_BLEND); break;
	case 1: glEnable(GL_BLEND); break;
	case 2: glDisable(GL_BLEND); break;
	}

	glClear(GL_DEPTH_BUFFER_BIT);// lösche Tiefen-Informationen
	glClearColor(0.95f, 0.95f, 0.95f, 0);// setze Lösch-Farbe
	glClear(GL_COLOR_BUFFER_BIT);// lösche Hintergrund
	
	private_coins = coins; private_robuttons = robuttons;
	//private copy to avoid moving while displayinng
	/*
	if(private_coins!=NULL) delete[] private_coins;
	if(private_robuttons!=NULL) delete[] private_robuttons;
	private_coins = new Coin[coinCount];
	private_robuttons = new Robutton[robuttonCount];
	for(int i=0; i<coinCount; i++)
		private_coins[i] = coins[i];
	for(int i=0; i<robuttonCount; i++)
		private_robuttons[i] = robuttons[i];
	*/
	glLoadIdentity();
	glColor3f(0,0,0);// schwarzer Rahmen
	glBegin(GL_QUADS);
		glVertex2f(0/scale, 0/scale);
		glVertex2f(0/scale, tableHeight);
		glVertex2f(tableWidth, tableHeight);
		glVertex2f(tableWidth, 0/scale);
	glEnd();
	glColor3f(1,1,1);// weißer Tisch
	glBegin(GL_QUADS);
		glVertex2f(1/scale, 1/scale);
		glVertex2f(1/scale, tableHeight-1/scale);
		glVertex2f(tableWidth-1/scale, tableHeight-1/scale);
		glVertex2f(tableWidth-1/scale, 1/scale);
	glEnd();

	glPointSize(2*scale);
	//Münzen
	if(outline) {// Rand
		glColor3f(0,0,0); // schwarz
		if(aa!=2) glBegin(GL_POINTS); // zur Verhinderung des Ersten GROßEN
		for(int i=0; i<coinCount; i++)
			if(aa!=2) glVertex2f(private_coins[i].x, private_coins[i].y);
			else circlePolygon(private_coins[i].x, private_coins[i].y);
			if(aa!=2) glEnd();
			glPointSize(2*scale-2);
	}
	glColor3f(1.0f, 1.0f, 0); // gelb
	if(aa!=2) glBegin(GL_POINTS);// zur Verhinderung des Ersten GROßEN
	for(int i=0; i<coinCount; i++)
		if(aa!=2) glVertex2f(private_coins[i].x, private_coins[i].y);
		else if(!outline) circlePolygon(private_coins[i].x, private_coins[i].y);
		else circlePolygon(private_coins[i].x, private_coins[i].y,0,1-1/scale);
		if(aa!=2) glEnd();
		//Robuttons
		if(outline) {// Rand
			glPointSize(2*scale);
			glColor3f(0,0,0); // schwarz
			if(aa!=2) glBegin(GL_POINTS); // zur Verhinderung des Ersten GROßEN
			for(int i=0; i<robuttonCount; i++)
				if(aa!=2) glVertex2f(private_robuttons[i].x, private_robuttons[i].y);
				else circlePolygon(private_robuttons[i].x, private_robuttons[i].y);
				if(aa!=2) glEnd();
				glPointSize(2*scale-2);
		}
		if(aa!=2) glBegin(GL_POINTS);// zur Verhinderung des Ersten GROßEN
		for(int i=0; i<robuttonCount; i++) {
			if(private_robuttons[i].carries) glColor3f(1.0f, 0, 0); // rot
			else if(private_robuttons[i].blocked) glColor3f(1.0f, 1.0f, 1.0f); // weiß
			else glColor3f(0, 1.0f, 0); // grün
			if(aa!=2) glVertex2f(private_robuttons[i].x, private_robuttons[i].y);
			else {
				if(!outline) circlePolygon(private_robuttons[i].x, private_robuttons[i].y);
				else circlePolygon(private_robuttons[i].x, private_robuttons[i].y,0,1-1/scale);
			}
		}
		if(aa!=2) glEnd();
		//Augen
		glColor3f(0,0,0);
		glPointSize(0.2*scale);
		glBegin(GL_POINTS);
		for(int i=0; i<robuttonCount; i++)
			glVertex2f(private_robuttons[i].x+0.7*sin(private_robuttons[i].angle * 3.14159265 / 180.0 ),
			private_robuttons[i].y+0.7*cos(private_robuttons[i].angle * 3.14159265 / 180.0 ));
		glEnd();

	times[8] = t.elapsed();

	swapBuffers();

	//glFinish(); //glFlush();// Ausgabepipeline synchronisieren
	/*
	//glGetIntegerv (GL_VIEWPORT, viewport);
	glClear(GL_ACCUM_BUFFER_BIT);
	for (int jitter = 0; jitter < 1; jitter++)
	{
	glPushMatrix ();
	*      Note that 4.5 is the distance in world space between
	*      left and right and bottom and top.
	*      This formula converts fractional pixel movement to
	*      world coordinates.
	/
	glTranslatef(1-1/scale, 1-1/scale, 1);
	drawScene();
	glPopMatrix ();
	glAccum(GL_ACCUM, 1.0 / 4);
	}
	glAccum (GL_RETURN, 1.0);
	glFlush();
	*/
	times[1]++; //draw call counter
	times[2] = t.elapsed();
}
const double PI2 = 2*3.1415926535897932384626433832795;
void SimulationWidget::circlePolygon(float center_x, float center_y, float z, float radius)
{
	float x = 0.0f;
	float y = 0.0f;
	glBegin(GL_POLYGON);
	// entgegen dem Uhrzeigersinn zeichnet die Vorderseite
	for( double theta = 0.0; theta > -PI2; theta -= PI2 / corners )
	{
		x = center_x + ( cos( theta ) * radius );
		y = center_y + ( sin( theta ) * radius);
		glVertex3f( x, y, z );
	}
	glEnd();
}
//Simulation starten
//Argumente: Anzahl der zu erstellenden Münzen und Robuttons
void SimulationWidget::start(int createCoinCount, int createRobuttonsCount)
{
	//Münzen erstellen
	coins = new Coin[createCoinCount];
	//Münzen in Reihen anordnen
	int row = 0;
	int column = 0;
	//Alle Münzen einzeln positionieren
	for(int i=0;i<createCoinCount;i++)
	{
		coins[i].x = 1.2+(2.2*column);
		coins[i].y = 1.2+(2.2*row);
		//coins[i].dropped=false;
		column++;
		//Nächste Reihe anfangen, wenn die Münze nicht mehr auf dem Tisch läge
		if(2+(column*2.2)>=tableWidth)
		{
			column=0;
			row++;
		}
	}
	coinCount=createCoinCount;

	//Robuttons erstellen
	robuttons = new Robutton[createRobuttonsCount];

	row = this->tableHeight / 3;
	column = 0;
	//Robuttons in Reihen anordnen, unter den Münzen
	for(int i=0;i<createRobuttonsCount;i++)
	{
		robuttons[i].x = 1.2+(2.2*column);
		robuttons[i].y = 1.2+(2.2*row);
		//Zufällige »Blickrichtung«
		robuttons[i].angle = qrand() % 360;
		//robuttons[i].carries = false;
		//robuttons[i].blocked = false;
		column++;
		//Nächste Reihe anfangen, wenn der Robutton sich nicht mehr auf dem Tisch befände
		if(2+(column*2.2)>=tableWidth)
		{
			column=0;
			row--;
		}
	}
	robuttonCount=createRobuttonsCount;

	//zum ersten mal sektorieren
	computeSectors();

	//Timer der Simulation starten
	calcer->resume();
	displayer->resume();
	informer->resume();
	times[0] = times[1] = 0;
	running=true;
}
//Simulation beenden
void SimulationWidget::stop()
{
	pause();
	while(calcer->working()) Sleep(1);
	statusMessage("Simulation beenden...");
	coinCount=0;
	robuttonCount=0;
	running=false;
	delete[] coins;
	delete[] robuttons;
	eraseSectors();
	statusMessage("Simulation beendet.");
	update();// free drawing panel
}
//Simulation unterbrechen
void SimulationWidget::pause()
{
	calcer->pause();
	displayer->pause();
	displayer->immediateUpdate();
}
//Simulation fortführen
void SimulationWidget::resume()
{
	calcer->resume();
	displayer->resume();
}
//Bewegungsfunktion mit überprüfung auf alle Kollisionen
void SimulationWidget::moveSector_global()
{
	QTime t;
	t.start();

	int privateSectorSize = sectorSize;

	//für jeden Robutton:
	for(int i=0;i<robuttonCount;i++) {
		sectors[(int)robuttons[i].x/privateSectorSize][(int)robuttons[i].y/privateSectorSize].robuttons.removeOne(&robuttons[i]);
		robuttons[i].moveStep();
		sectors[(int)robuttons[i].x/privateSectorSize][(int)robuttons[i].y/privateSectorSize].addRobutton(robuttons[i]);
	}

	times[4] = unused->coins.size();
	times[5] = unused->robuttons.size();

	times[6] = t.elapsed();

	//kollision mit den Tischkanten:
	//Wenn der Abstand zwischen Mittelpunkt des Robuttons und einer Kante kleiner
	//ist als 1 (Radius), so wird dieser Abstand auf 1 gesetzt und der Robutton
	//wird reflektiert (Einfallswinkel = Ausfallswinkel) und bewegt sich einen
	//weiteren Schritt
	Robutton *robutton;
	for(int x=0; x<tableWidth/privateSectorSize; x++)
	{
		for(int r=0; r<sectors[x][0].robuttons.size(); r++) //oben
		{
			robutton = sectors[x][0].robuttons[r];
			sectors[x][0].robuttons.removeAt(r);
			if(robutton->y-1<=0)
			{
				robutton->angle=180-robutton->angle;
				robutton->y=1; //reset to the borderline
				robutton->moveStep();
			}
			sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
		}
		for(int r=0; r<sectors[x][(tableHeight-1)/privateSectorSize].robuttons.size(); r++) //unten
		{
			robutton = sectors[x][(tableHeight-1)/privateSectorSize].robuttons[r];
			sectors[x][(tableHeight-1)/privateSectorSize].robuttons.removeAt(r);
			if(robutton->y+1>=tableHeight)
			{
				robutton->angle=180-robutton->angle;
				robutton->y=tableHeight-1; //reset to the borderline
				robutton->moveStep();
			}
			sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
		}
	}
	for(int y=0; y<tableHeight/privateSectorSize; y++)
	{
		for(int r=0; r<sectors[0][y].robuttons.size(); r++) //links
		{
			robutton = sectors[0][y].robuttons[r];
			sectors[0][y].robuttons.removeAt(r);
			if(robutton->x-1<=0)
			{
				robutton->angle=360-robutton->angle;
				robutton->x=1; //reset to the borderline
				robutton->moveStep();
			}
			sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
		}
		for(int r=0; r<sectors[(tableWidth-1)/privateSectorSize][y].robuttons.size(); r++) //rechts
		{
			robutton = sectors[(tableWidth-1)/privateSectorSize][y].robuttons[r];
			sectors[(tableWidth-1)/privateSectorSize][y].robuttons.removeAt(r);
			if(robutton->x+1>=tableWidth)
			{
				robutton->angle=360-robutton->angle;
				robutton->x=tableWidth-1; //reset to the borderline
				robutton->moveStep();
			}
			sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
		}
	}
	times[9]=0;
	//Abstand zweier Robuttons, Winkel zwischen ihnen
	double distance, direction;
	int xi, xl, y, yl;
	bool collision, coinSet;
	Robutton *robutton2;
	Coin *coin;
	for(int r=0; r<robuttonCount; r++)
	{
		robutton = &robuttons[r];
		collision=false;
		coinSet=false;
		if(privateSectorSize==1) {
			xi = (((int)robutton->x-2<0)?0:((int)robutton->x-2));
			y = (((int)robutton->y-2<0)?0:((int)robutton->y-2));
			if((xl=(int)robutton->x+2)>=tableWidth) xl=tableWidth-1;
			if((yl=(int)robutton->y+2)>=tableHeight) yl=tableHeight-1;
		} else {
			xi = (((int)robutton->x/privateSectorSize-1<0)?0:((int)robutton->x/privateSectorSize-1));
			y = (((int)robutton->y/privateSectorSize-1<0)?0:((int)robutton->y/privateSectorSize-1));
			if((xl=(int)robutton->x/privateSectorSize+1)>=tableWidth/privateSectorSize) xl=tableWidth/privateSectorSize-1;
			if((yl=(int)robutton->y/privateSectorSize+1)>=tableHeight/privateSectorSize) yl=tableHeight/privateSectorSize-1;
		}

		for(;xi <= xl; xi++)
		for(int yi=y; yi <= yl; yi++)
		{
			//Kollision zweier Robuttons
			for(int rc=0;rc<sectors[xi][yi].robuttons.size();rc++)
			{
				times[9]++;
				robutton2 = sectors[xi][yi].robuttons[rc];
				if(robutton2!=robutton) //Keine Kollision mit sich selbst
				{
					//Abstand der zwei Robuttons über Pythagoras
					distance = (robutton->x-robutton2->x)*(robutton->x-robutton2->x)
						  + (robutton->y-robutton2->y)*(robutton->y-robutton2->y);
					//Falls der Abstand kleiner als der Durchmesser (2) ist…
					if(distance<4)
					{
					//Robuttons von der Kollisionstelle reflektieren und random bewegen
					//oben - do collision stuff only with upper robutton
						if(robutton->y < robutton2->y)
						{
							distance = 2.0 / qSqrt(distance);
							//Robuttons wieder auseinander schieben – auf Abstand=2
							sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].robuttons.removeOne(robutton);
							robutton->x=robutton2->x + (robutton->x-robutton2->x) * distance;
							robutton->y=robutton2->y + (robutton->y-robutton2->y) * distance;
							sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
							//direct robbuttons on collision point normal
							if(true)//alternate collisions
							{
								//Winkel zwischen den Robuttons
								direction = qAtan( (robutton->x-robutton2->x)
												   /(robutton->y-robutton2->y) ) / 3.14159265*180.0;
								//links
								if(robutton->x < robutton2->x)
								{
									robutton->angle = direction + 180;
									robutton2->angle = direction;
								}
								//rechts
								else if(robutton->x > robutton2->x)
								{
									robutton->angle = 180 + direction;
									robutton2->angle = direction;
								}
							}
							else
							{
								//Robuttons um 180° drehen
								robutton->angle += 180;
								robutton2->angle += 180;
							}
							//Robuttons um zufälligen Winkel zwischen -90° und +90° drehen
							robutton->angle+=(qrand()%180)-90;
							robutton2->angle+=(qrand()%180)-90;
						}
					}
				}
			}

			//Kollision Robutton-Münze
			//Überprüfe mit allen Münzen
			for(int cc=0;cc<sectors[xi][yi].coins.size();cc++)
			{
				times[9]++;
				coin = sectors[xi][yi].coins[cc];
				//ist der Abstand (mit Pythagoras berechnet) kleiner als der Durchmesser
				if( (distance = (robutton->x-coin->x)*(robutton->x-coin->x)
							  + (robutton->y-coin->y)*(robutton->y-coin->y))
					< 4)
				{
					//…dann existiert eine Kollision
					collision=true;
					//Verhalten je nach Zustand
					//hat trägt der Robutton bereits eine Münze
					if(robutton->carries==true && robutton->blocked==false)//verhindert Ablegen unmittelbar nach Aufnahme
					{
						distance = 2.0 / qSqrt(distance);
						sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].robuttons.removeOne(robutton);
						robutton->x = coin->x + (robutton->x-coin->x) * distance;
						robutton->y = coin->y + (robutton->y-coin->y) * distance;
						sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
						//soll er eine Münze ablegen. Hierzu muss im Münz-Array gesucht werden,
						//welche Münze negative Koordinaten hat, sodass sie wieder gesetzt werden kann
						coinSet=false;
						for(int c=0;c<unused->coins.size();c++)
						{
							if(unused->coins[c]->x<-15 && coinSet==false)
							{
								//Ihre Koordinaten werden auf die des Robuttons gesetzt und die Suche
								//wird abgebrochen
								unused->coins[c]->x=robutton->x;
								unused->coins[c]->y=robutton->y;
								//TODO ?Exception Robutton xy <0 >xy
								sectors[(int)unused->coins[c]->x/privateSectorSize][(int)unused->coins[c]->y/privateSectorSize].addCoin(*unused->coins[c]);
								unused->coins.removeAt(c);
								coinSet=true;
							}
						}
						//Der Robutton dreht sich um 180°
						robutton->angle+=180;
						//Der Robutton trägt keine Münze mehr, wechselt in den Zustand »block«
						robutton->carries=false;
						robutton->blocked=true;
					}
					//trägt er keine Münze,
					else
					//und ist auch nicht im Zustand »block«,
					if(robutton->blocked==false && distance < pow(2*0.5,2))//Aufnahmefläche verringern
					{
						//dann nimmt er die Münze auf, ihre Koordinaten werden auf -100,-100 gesetzt
						robutton->carries=true;
						robutton->blocked=true;
						sectors[xi][yi].coins.removeAt(cc);
						coin->x=-100;
						coin->y=-100;
						unused->addCoin(*coin);
					}
				}
			}
		}
		//wenn keine Kollision existiert und der Robutton im Zustand »block« ist, verlässt er diesen
		if(!collision && robutton->blocked)
			robutton->blocked=false;
	}
	times[3] = times[9];
	times[7] = t.elapsed();
}
void SimulationWidget::moveSector_local()
{
	QTime t;
	t.start();

	int privateSectorSize = sectorSize;

	//für jeden Robutton:
	for(int i=0;i<robuttonCount;i++) {
		sectors[(int)robuttons[i].x/privateSectorSize][(int)robuttons[i].y/privateSectorSize].robuttons.removeOne(&robuttons[i]);
		robuttons[i].moveStep();
		sectors[(int)robuttons[i].x/privateSectorSize][(int)robuttons[i].y/privateSectorSize].addRobutton(robuttons[i]);
	}

	times[4] = unused->coins.size();
	times[5] = unused->robuttons.size();

	times[6] = t.elapsed();

	//kollision mit den Tischkanten:
	//Wenn der Abstand zwischen Mittelpunkt des Robuttons und einer Kante kleiner
	//ist als 1 (Radius), so wird dieser Abstand auf 1 gesetzt und der Robutton
	//wird reflektiert (Einfallswinkel = Ausfallswinkel) und bewegt sich einen
	//weiteren Schritt
	Robutton *robutton;
	for(int x=0; x<tableWidth/privateSectorSize; x++)
	{
		for(int r=0; r<sectors[x][0].robuttons.size(); r++) //oben
		{
			robutton = sectors[x][0].robuttons[r];
			sectors[x][0].robuttons.removeAt(r);
			if(robutton->y-1<=0)
			{
				robutton->angle=180-robutton->angle;
				robutton->y=1; //reset to the borderline
				robutton->moveStep();
			}
			sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
		}
		for(int r=0; r<sectors[x][(tableHeight-1)/privateSectorSize].robuttons.size(); r++) //unten
		{
			robutton = sectors[x][(tableHeight-1)/privateSectorSize].robuttons[r];
			sectors[x][(tableHeight-1)/privateSectorSize].robuttons.removeAt(r);
			if(robutton->y+1>=tableHeight)
			{
				robutton->angle=180-robutton->angle;
				robutton->y=tableHeight-1; //reset to the borderline
				robutton->moveStep();
			}
			sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
		}
	}
	for(int y=0; y<tableHeight/privateSectorSize; y++)
	{
		for(int r=0; r<sectors[0][y].robuttons.size(); r++) //links
		{
			robutton = sectors[0][y].robuttons[r];
			sectors[0][y].robuttons.removeAt(r);
			if(robutton->x-1<=0)
			{
				robutton->angle=360-robutton->angle;
				robutton->x=1; //reset to the borderline
				robutton->moveStep();
			}
			sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
		}
		for(int r=0; r<sectors[(tableWidth-1)/privateSectorSize][y].robuttons.size(); r++) //rechts
		{
			robutton = sectors[(tableWidth-1)/privateSectorSize][y].robuttons[r];
			sectors[(tableWidth-1)/privateSectorSize][y].robuttons.removeAt(r);
			if(robutton->x+1>=tableWidth)
			{
				robutton->angle=360-robutton->angle;
				robutton->x=tableWidth-1; //reset to the borderline
				robutton->moveStep();
			}
			sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
		}
	}
	times[9]=0;
	//Abstand zweier Robuttons, Winkel zwischen ihnen
	double distance, direction;
	int xi, xl, y, yl;
	bool collision, coinSet;
	Robutton *robutton2;
	Coin *coin;
	for(int r=0; r<robuttonCount; r++)
	{
		robutton = &robuttons[r];
		collision=false;
		coinSet=false;
		if(privateSectorSize==1) {
			xi = (((int)robutton->x-2<0)?0:((int)robutton->x-2));
			y = (((int)robutton->y-2<0)?0:((int)robutton->y-2));
			if((xl=(int)robutton->x+2)>=tableWidth) xl=tableWidth-1;
			if((yl=(int)robutton->y+2)>=tableHeight) yl=tableHeight-1;
		} else {
			xi = (((int)robutton->x/privateSectorSize-1<0)?0:((int)robutton->x/privateSectorSize-1));
			y = (((int)robutton->y/privateSectorSize-1<0)?0:((int)robutton->y/privateSectorSize-1));
			if((xl=(int)robutton->x/privateSectorSize+1)>=tableWidth/privateSectorSize) xl=tableWidth/privateSectorSize-1;
			if((yl=(int)robutton->y/privateSectorSize+1)>=tableHeight/privateSectorSize) yl=tableHeight/privateSectorSize-1;
		}

		for(;xi <= xl; xi++)
			for(int yi=y; yi <= yl; yi++)
			{
				//Kollision zweier Robuttons
				for(int rc=0;rc<sectors[xi][yi].robuttons.size();rc++)
				{
					times[9]++;
					robutton2 = sectors[xi][yi].robuttons[rc];
					if(robutton2!=robutton) //Keine Kollision mit sich selbst
					{
						//Abstand der zwei Robuttons über Pythagoras
						distance = (robutton->x-robutton2->x)*(robutton->x-robutton2->x)
							+ (robutton->y-robutton2->y)*(robutton->y-robutton2->y);
						//Falls der Abstand kleiner als der Durchmesser (2) ist…
						if(distance<4)
						{
							//Robuttons von der Kollisionstelle reflektieren und random bewegen
							//oben - do collision stuff only with upper robutton
							if(robutton->y < robutton2->y)
							{
								distance = 2.0 / qSqrt(distance);
								//Robuttons wieder auseinander schieben – auf Abstand=2
								sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].robuttons.removeOne(robutton);
								robutton->x=robutton2->x + (robutton->x-robutton2->x) * distance;
								robutton->y=robutton2->y + (robutton->y-robutton2->y) * distance;
								sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
								//direct robbuttons on collision point normal
								if(true)//alternate collisions
								{
									//Winkel zwischen den Robuttons
									direction = qAtan( (robutton->x-robutton2->x)
										/(robutton->y-robutton2->y) ) / 3.14159265*180.0;
									//links
									if(robutton->x < robutton2->x)
									{
										robutton->angle = direction + 180;
										robutton2->angle = direction;
									}
									//rechts
									else if(robutton->x > robutton2->x)
									{
										robutton->angle = 180 + direction;
										robutton2->angle = direction;
									}
								}
								else
								{
									//Robuttons um 180° drehen
									robutton->angle += 180;
									robutton2->angle += 180;
								}
								//Robuttons um zufälligen Winkel zwischen -90° und +90° drehen
								robutton->angle+=(qrand()%180)-90;
								robutton2->angle+=(qrand()%180)-90;
							}
						}
					}
				}

				//Kollision Robutton-Münze
				//Überprüfe mit allen Münzen
				for(int cc=0;cc<sectors[xi][yi].coins.size();cc++)
				{
					times[9]++;
					coin = sectors[xi][yi].coins[cc];
					//ist der Abstand (mit Pythagoras berechnet) kleiner als der Durchmesser
					if( (distance = (robutton->x-coin->x)*(robutton->x-coin->x)
						+ (robutton->y-coin->y)*(robutton->y-coin->y))
						< 4)
					{
						//…dann existiert eine Kollision
						collision=true;
						//Verhalten je nach Zustand
						//hat trägt der Robutton bereits eine Münze
						if(robutton->carries==true && robutton->blocked==false)//verhindert Ablegen unmittelbar nach Aufnahme
						{
							distance = 2.0 / qSqrt(distance);
							sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].robuttons.removeOne(robutton);
							robutton->x = coin->x + (robutton->x-coin->x) * distance;
							robutton->y = coin->y + (robutton->y-coin->y) * distance;
							sectors[(int)robutton->x/privateSectorSize][(int)robutton->y/privateSectorSize].addRobutton(*robutton);
							//soll er eine Münze ablegen. Hierzu muss im Münz-Array gesucht werden,
							//welche Münze negative Koordinaten hat, sodass sie wieder gesetzt werden kann
							coinSet=false;
							for(int c=0;c<unused->coins.size();c++)
							{
								if(unused->coins[c]->x<-15 && coinSet==false)
								{
									//Ihre Koordinaten werden auf die des Robuttons gesetzt und die Suche
									//wird abgebrochen
									unused->coins[c]->x=robutton->x;
									unused->coins[c]->y=robutton->y;
									//TODO ?Exception Robutton xy <0 >xy
									sectors[(int)unused->coins[c]->x/privateSectorSize][(int)unused->coins[c]->y/privateSectorSize].addCoin(*unused->coins[c]);
									unused->coins.removeAt(c);
									coinSet=true;
								}
							}
							//Der Robutton dreht sich um 180°
							robutton->angle+=180;
							//Der Robutton trägt keine Münze mehr, wechselt in den Zustand »block«
							robutton->carries=false;
							robutton->blocked=true;
						}
						//trägt er keine Münze,
						else
							//und ist auch nicht im Zustand »block«,
							if(robutton->blocked==false && distance < pow(2*0.5,2))//Aufnahmefläche verringern
							{
								//dann nimmt er die Münze auf, ihre Koordinaten werden auf -100,-100 gesetzt
								robutton->carries=true;
								robutton->blocked=true;
								sectors[xi][yi].coins.removeAt(cc);
								coin->x=-100;
								coin->y=-100;
								unused->addCoin(*coin);
							}
					}
				}
			}
			//wenn keine Kollision existiert und der Robutton im Zustand »block« ist, verlässt er diesen
			if(!collision && robutton->blocked)
				robutton->blocked=false;
	}
	times[3] = times[9];
	times[7] = t.elapsed();
}
void SimulationWidget::moveBruteForce()
{
	QTime t;
	t.start();

	//für jeden Robutton:
	for(int i=0;i<robuttonCount;i++)
	{
		//normalisieren der Winkel
		while(robuttons[i].angle<0)
			robuttons[i].angle+=360;
		while(robuttons[i].angle>360)
			robuttons[i].angle-=360;
		/*
		if(robuttons[i].angle>0 && robuttons[i].angle<180)
			robuttons[i].angle += 2;
		else if(robuttons[i].angle<360 && robuttons[i].angle>180)
			robuttons[i].angle -= 2;
		*/
		//1 Step bewegen
		robuttons[i].x=robuttons[i].x+step*sin(robuttons[i].angle* rad );
		robuttons[i].y=robuttons[i].y+step*cos(robuttons[i].angle* rad );
	}
	times[6] = t.elapsed();
	//kollision mit den Tischkanten:
	//Wenn der Abstand zwischen Mittelpunkt des Robuttons und einer Kante kleiner
	//ist als 1 (Radius), so wird dieser Abstand auf 1 gesetzt und der Robutton
	//wird reflektiert (Einfallswinkel = Ausfallswinkel) und bewegt sich einen
	//weiteren Schritt
	for(int i=0;i<robuttonCount;i++)
	{
		if(robuttons[i].x+1>=tableWidth)
		{
		  robuttons[i].angle=360-robuttons[i].angle;
		  robuttons[i].x=tableWidth-1+step*sin(robuttons[i].angle* rad );
		}
		if(robuttons[i].x-1<=0)
		{
		  robuttons[i].angle=360-robuttons[i].angle;
		  robuttons[i].x=1+step*sin(robuttons[i].angle* rad );
		}
		if(robuttons[i].y+1>=tableHeight)
		{
		  robuttons[i].angle=180-robuttons[i].angle;
		  robuttons[i].y=tableHeight-1+step*cos(robuttons[i].angle* rad );
		}
		if(robuttons[i].y-1<=0)
		{
		  robuttons[i].angle=180-robuttons[i].angle;
		  robuttons[i].y=1+step*cos(robuttons[i].angle* rad );
		}
	}
	times[9]=0;
  //Abstand zweier Robuttons, Winkel zwischen ihnen
  double distance, direction;

  //Kollision zweier Robuttons
  for(int i=0;i<robuttonCount;i++)
  {
	for(int j=0;j<robuttonCount;j++)
	{
		times[9]++;
	  if(j!=i) //Keine kollision mit sich selbst
	  {
		//Abstand der zwei Robuttons über Pythagoras
		distance = (robuttons[i].x-robuttons[j].x)*(robuttons[i].x-robuttons[j].x)
				+ (robuttons[i].y-robuttons[j].y)*(robuttons[i].y-robuttons[j].y);
		//Falls der Abstand kleiner als der Durchmesser (2) ist…
		if(distance<4)
		{
		  //Robuttons von der Kollisionstelle reflektieren und random bewegen
		  //oben - do collision stuff only with upper robutton
		  if(robuttons[i].y < robuttons[j].y)
		  {
			  distance = 2.0 / qSqrt(distance);
			  //Robuttons wieder auseinander schieben – auf Abstand=2
			  robuttons[i].x=robuttons[j].x + (robuttons[i].x-robuttons[j].x) * distance;
			  robuttons[i].y=robuttons[j].y + (robuttons[i].y-robuttons[j].y) * distance;
			  //direct robbuttons on collision point normal
			  if(alterCollisionRules)
			  {
				  //Winkel zwischen den Robuttons
				  direction = qAtan( (robuttons[i].x-robuttons[j].x)/(robuttons[i].y-robuttons[j].y) ) / 3.14159265*180.0;
				  //links
				  if(robuttons[i].x < robuttons[j].x)
				  {
					 robuttons[i].angle = direction + 180;
					 robuttons[j].angle = direction;
				  }
				  //rechts
				  else if(robuttons[i].x > robuttons[j].x)
				  {
					 robuttons[i].angle = 180 + direction;
					 robuttons[j].angle = direction;
				  }
			  }
			  else
			  {
				  //Robuttons um 180° drehen
				  robuttons[i].angle += 180;
				  robuttons[j].angle += 180;
			  }
			  //Robuttons um zufälligen Winkel zwischen -90° und +90° drehen
			  robuttons[i].angle+=(qrand()%180)-90;
			  robuttons[j].angle+=(qrand()%180)-90;
		  }
		}
	  }
	}
  }
  //Kollision Robutton-Münze

  bool collision;
  bool coinSet=false;

  //Für alle Robuttons
  for(int i=0;i<robuttonCount;i++)
  {
	collision=false;
	//Überprüfe mit allen Münzen
	for(int j=0;j<coinCount;j++)
	{
		times[9]++;
	  //ist der Abstand (mit Pythagoras berechnet) kleiner als der Durchmesser
	  if( (distance = (robuttons[i].x-coins[j].x)*(robuttons[i].x-coins[j].x)
					+ (robuttons[i].y-coins[j].y)*(robuttons[i].y-coins[j].y))
		  < 4) {
		//…dann existiert eine Kollision
		collision=true;

		//Verhalten je nach Zustand

		//hat trägt der Robutton bereits eine Münze
		if(robuttons[i].carries==true
		   && robuttons[i].blocked==false)//verhindert Ablegen unmittelbar nach Aufnahme
		{
			distance = 2.0 / qSqrt(distance);
		  robuttons[i].x = coins[j].x + (robuttons[i].x-coins[j].x) * distance;
		  robuttons[i].y = coins[j].y + (robuttons[i].y-coins[j].y) * distance;
		  //soll er eine Münze ablegen. Hierzu muss im Münz-Array gesucht werden,
		  //welche Münze negative Koordinaten hat, sodass sie wieder gesetzt werden kann
		  coinSet=false;
		  for(int k=0;k<coinCount;k++)
		  {
			if(coins[k].x<-15&&coinSet==false)
			{
			  //Ihre Koordinaten werden auf die des Robuttons gesetzt und die Suche
			  //wird abgebrochen
			  coins[k].x=robuttons[i].x;
			  coins[k].y=robuttons[i].y;
			  coinSet=true;
			}
		  }
		  //Der Robutton dreht sich um 180°
		  robuttons[i].angle+=180;
		  //Der Robutton trägt keine Münze mehr, wechselt in den Zustand »block«
		  robuttons[i].carries=false;
		  robuttons[i].blocked=true;
		}
		//trägt er keine Münze,
		else
		  //und ist auch nicht im Zustand »block«,
		  if(robuttons[i].blocked==false
			 && distance < pow(2*0.5,2))//Aufnahmefläche verringern
		  {
			//dann nimmt er die Münze auf, ihre Koordinaten werden auf -100,-100 gesetzt
			robuttons[i].carries=true;
			robuttons[i].blocked=true;
			coins[j].x=-100;
			coins[j].y=-100;
		  }
	  }
	}
	//wenn keine Kollision existiert und der Robutton im Zustand »block« ist, verlässt er diesen
	if(!collision && robuttons[i].blocked)
	{
	  robuttons[i].blocked=false;
	}
  }

  times[3] = times[9];
  times[7] = t.elapsed();
}
void SimulationWidget::resize() {
	scale = ((double)width()-1)/(double)tableWidth;
	if(tableHeight*scale > height()) {
		scale=((double)height()-2)/(double)tableHeight;
		movex = (width()-scale*tableWidth)/2;
		movey = 0;
	}
	else {
		movex = 0;
		movey = (height()-scale*tableHeight) / 2;
	}
	displayer->changeViewport();
}
//Globale Variable Tischhöhe aktualisieren
void SimulationWidget::updateTableHeight(int i) {
	tableHeight=i;
	resize();
}
//Globale Variable Tischbreite aktualisieren
void SimulationWidget::updateTableWidth(int i) {
	tableWidth=i;
	resize();
}
int SimulationWidget::antiAlias(){
	return aa;
}
void SimulationWidget::antiAlias(int flag){
	aa = flag;
	/*
	if(aa==2 && !format().sampleBuffers()) {
		displayer->pause();
		if(displayer->working()) Sleep(1);
		QGLFormat newFormat;
		newFormat.setSampleBuffers(aa==2);
		this->setFormat(newFormat);
		displayer->initGL();
		displayer->changeViewport();
		displayer->resume();
	} if(aa!=2 && format().sampleBuffers()) {
		displayer->pause();
		if(displayer->working()) Sleep(1);
		QGLFormat newFormat;
		newFormat.setSampleBuffers(aa==2);
		this->setFormat(newFormat);
		displayer->initGL();
		displayer->changeViewport();
		displayer->resume();
	} else update();
	*/
}
bool SimulationWidget::shadowValue(){
	return shdw;
}
void SimulationWidget::shadowValue(bool flag){
	shdw = flag;
	update();
}
bool SimulationWidget::outlines(){
	return outline;
}
void SimulationWidget::outlines(bool flag){
	outline = flag;
	update();
}
bool SimulationWidget::alterCollisionRule(){
	return alterCollisionRules;
}
void SimulationWidget::alterCollisionRule(bool flag) {
	alterCollisionRules = flag;
	update();
}
void SimulationWidget::eraseSectors() {
	statusMessage("Simulation beenden...Sektoren löschen");
	if(sectorSize==0 || sectors==NULL || unused==NULL) return;
	//Sektoren löschen
	for(int i=0; i<(tableWidth+1)/sectorSize; i++) {
		if(i%100==0) statusMessage("Simulation beenden...Sektoren löschen: "
			+QVariant(i).toString()+"/"+QVariant(tableWidth/sectorSize).toString());
		delete[] sectors[i];
	}
	delete[] sectors;
	delete unused;
	sectors = NULL;
	unused = NULL;
	statusMessage("Simulation beenden...Sektoren gelöscht");
}
void SimulationWidget::requestSectorChoice(int flag) {
	calcer->computeSectors(flag);
	update();
}
void SimulationWidget::setSectorChoice(int flag) {
	this->sectorChoice = flag;
	if(flag>1) sectorSize = flag;
	else sectorSize = 1;
}
void SimulationWidget::computeSectors() {
	if(sectorChoice == 0) return;
	//Sektoren neu erstellen
	int size = tableWidth/sectorSize+1;
	sectors = new Sector*[size];//TODO +1?
	for(int i=0; i<size; i++)
		sectors[i] = new Sector[tableHeight/sectorSize+1];
	if(running && unused != NULL)
		delete unused;
	unused = new Sector;

	//erneut sektorieren
	for(int i=0; i<robuttonCount; i++) {
		if(robuttons[i].x<0 || robuttons[i].x>=tableWidth
			|| robuttons[i].y<0 || robuttons[i].y>=tableHeight)
			unused->addRobutton(robuttons[i]);
		else sectors[(int)robuttons[i].x/sectorSize][(int)robuttons[i].y/sectorSize].addRobutton(robuttons[i]);
	}
	for(int i=0; i<coinCount; i++)
		if(coins[i].x<0 || coins[i].x>=tableWidth
			|| coins[i].y<0|| coins[i].y>=tableHeight)
			unused->addCoin(coins[i]);
		else
			sectors[(int)coins[i].x/sectorSize][(int)coins[i].y/sectorSize].addCoin(coins[i]);

	//if(flag!=0) {
	//	//if(running) eraseSectors();
	//	//Sektoren neu erstellen
	//	int size = tableWidth/flag+1;
	//	Sector **help = new Sector*[size];//TODO +1?
	//	for(int i=0; i<size; i++)
	//		help[i] = new Sector[tableHeight/flag+1];
	//	if(running && unused != NULL)
	//		delete unused;
	//	unused = new Sector;
	//	//erneut sektorieren
	//	for(int i=0; i<robuttonCount; i++) {
	//		if(robuttons[i].x<0 || robuttons[i].x>=tableWidth
	//			|| robuttons[i].y<0 || robuttons[i].y>=tableHeight)
	//			unused->addRobutton(robuttons[i]);
	//		else help[(int)robuttons[i].x/flag][(int)robuttons[i].y/flag].addRobutton(robuttons[i]);
	//	}
	//	for(int i=0; i<coinCount; i++)
	//		if(coins[i].x<0 || coins[i].x>=tableWidth
	//			|| coins[i].y<0|| coins[i].y>=tableHeight)
	//			unused->addCoin(coins[i]);
	//		else
	//			help[(int)coins[i].x/flag][(int)coins[i].y/flag].addCoin(coins[i]);
	//	//delete old ones
	//	Sector **temp = sectors;
	//	int helps = sectorChoice;
	//	/*
	//	bool check=false;
	//	if(running) {
	//		if(!calcer->pauses()) {
	//			calcer->pause();
	//			check=true;
	//		}
	//		while (calcer->working()) Sleep(1);
	//	}
	//	*/
	//	sectors = help;
	//	sectorChoice = flag;
	//	//if(check) resume();
	//	if(running && helps!=0 && temp!=NULL) {
	//		for(int i=0; i<(tableWidth+1)/helps; i++)
	//			delete[] temp[i];
	//		delete[] temp;
	//		temp = NULL;
	//	}
	//}
}
int SimulationWidget::getSectorChoice() {
	return sectorChoice;
}
double SimulationWidget::getStepValue(){
	return step;
}
void SimulationWidget::setStepValue(double arg){
	step = arg;
}
int SimulationWidget::simTimerTime(){
	return simTime;
}
void SimulationWidget::simTimerTime(int arg){
	simTime = arg;
}
int SimulationWidget::getDisplayTimerTime(){
	return displayTime;
}
void SimulationWidget::setDisplayTimerTime(int arg){
	displayTime = arg;
}
int SimulationWidget::corner() {
	return corners;
}
void SimulationWidget::corner(int value) {
	corners = value;
	resize();
	//update();
}
void SimulationWidget::statusMessage(QString str) {
	emit showMessage(str);
}
