# Robuttons #
This is a solution to task 2 – round 1 of the 29th Bundeswettbewerb Informatik.  
Initial code and submission by [Florian Meinel](http://www.florianmeinel.de).  
Motivation for change was bad performance.  
Performance improvements by drawing with `QPainter` onto `QGLWidget` instead of `QWidget` motivated for a native OpenGL implementation utilizing `QGLWidget`.  
Brute force n-body simulation was split up into independent grid cells.  
The optimized version is orders of magnitude faster in simulation and rendering. (although it’s missing a smooth shadow below the *Robuttons* – from the original version – which may get added back in the future). 

## Build information ##
* this project uses `Qt5` and `CMake`
* use [msys2](http://sourceforge.net/projects/msys2/) to wipe out any pain you might have had when developing on windows
