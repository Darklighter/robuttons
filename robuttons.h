#ifndef ROBUTTONS_H
#define ROBUTTONS_H

namespace Robuttons
{
	class SimulationWidget;
	class Tfred;
	class Coin;
	class Robutton;
	class Sector;
}

#include <QList>
#include <QVariant>
#include <qmath.h>

static double step;
static double rad = 3.14159265/180.0;

class Robuttons::Coin {
public:
	Coin() {
		dropped=false;
	}
	Sector *sector;
	bool dropped;
	double x;
	double y;
	QString toString() {
		return "(" + QVariant(this->x).toString() + "," + QVariant(this->y).toString() + ")";
	}
};

class Robuttons::Robutton {
public:
	Robutton() {
		blocked=carries=false;
	}
	Sector *sector;
	double x;
	double y;
	double angle;
	bool blocked;
	bool carries;
	bool moveStep() {
		// normalisieren der Winkel
		while(angle<0) angle+=360;
		while(angle>360) angle-=360;
		/*// continuous rotation of robuttons
		if(robuttons[i].angle>0 && robuttons[i].angle<180)
			robuttons[i].angle += 2;
		else if(robuttons[i].angle<360 && robuttons[i].angle>180)
			robuttons[i].angle -= 2;
		*/
		//1 Step bewegen
		int xpre = (int)x, ypre = (int)y;
		x = x + step * sin(angle * rad);
		y = y + step * cos(angle * rad);
		return xpre-(int)x+ypre-(int)y!=0;
	}
	QString toString() {
	return "(" + QVariant(this->x).toString() + "," + QVariant(this->y).toString() + "):" + QVariant(angle).toString();
	}
};

class Robuttons::Sector {
public:
	QList<Robuttons::Robutton*> robuttons;
	QList<Robuttons::Coin*> coins;
	void clearSector() {
		robuttons.clear();
		coins.clear();
	}
	void addRobutton(Robuttons::Robutton &r) {
		robuttons.append(&r);
	}
	void addCoin(Robuttons::Coin &c) {
		coins.append(&c);
	}
};

#endif // ROBUTTONS_H
