#ifndef SimulationWidget_H
#define SimulationWidget_H

#include <QThread>
#include <QtGui>
#include <QWidget>
#include <QtOpenGL/QGLWidget>
#include <math.h>
#include "robuttons.h" //abstract classes

using namespace Robuttons;

class Robuttons::SimulationWidget : public QGLWidget {
	Q_OBJECT //needed to use signals and slots

public:
	int times[10];
	int simTime, displayTime;

	int tableWidth;
	int tableHeight;
	double scale;
	int movex, movey;

	SimulationWidget(QWidget *parent);
	~SimulationWidget();
	void start(int createCoinCount, int createRobuttonsCount);
	void stop();
	void pause();
	void resume();
	void resize();
	void circlePolygon(float center_x=0, float center_y=0, float z=0, float radius=1);
	int antiAlias();
	bool shadowValue();
	bool outlines();
	bool alterCollisionRule();
	int getSectorChoice();
	void computeSectors();
	void eraseSectors();
	double getStepValue();
	int simTimerTime();
	int getDisplayTimerTime();
	int corner();
	void statusMessage(QString);

	void draw();
	void drawTest();

	bool shdw;
	bool outline;
	int aa; //display flags
	int corners;
	bool alterCollisionRules; //optimized Robutton Collision from
	int sectorChoice;
	int sectorSize;
signals:
	void showMessage(QString str);

public slots:
	void updateTableWidth(int i);
	void updateTableHeight(int i);
	void antiAlias(int flag);
	void shadowValue(bool flag);
	void outlines(bool flag);
	void alterCollisionRule(bool flag);
	void requestSectorChoice(int flag);
	void setSectorChoice(int flag);
	void setStepValue(double value);
	void simTimerTime(int value);
	void setDisplayTimerTime(int value);
	void corner(int flag);
	void moveSector_global();
	void moveSector_local();
	void moveBruteForce();

protected:
	void resizeEvent(QResizeEvent*);
	void paintEvent(QPaintEvent*);

private:
	bool running;
	int coinCount;
	int robuttonCount;
	Coin *coins;
	Robutton *robuttons;
	Sector **sectors;
	Sector *unused;

	void createRobuttonsAndCoins(int createCoinCount, int createRobuttonsCount);

	Tfred *calcer, *displayer, *informer;
};

#endif
