#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[]) {
	QApplication app(argc, argv);
	app.setApplicationName("Robuttons");
	MainWindow mainwindow;
	return app.exec();
}
