#ifndef TFRED_H
#define TFRED_H

#include <QThread>
#include <QTimer>
#include "robuttons.h"

class Robuttons::Tfred : public QThread {
	Q_OBJECT //needed to use signals and slots
private:
	SimulationWidget *data;
	int id, sectorChoice;
	bool paused, works, kill, sectorFlag, resize, initNew;
	QTimer *timer;
	void run();
public:
	class WorkHints {
	public:
		static const int calcer = 0;
		static const int displayer = 1;
		static const int informer = 2;
	};
	Tfred(SimulationWidget *data, int id);
	void pause();
	void resume();
	bool working();
	bool pauses();
	void computeSectors(int);
	void initGL();
	void changeViewport();
	void immediateUpdate();
private:
	void glInit();
	void glResize();
private slots:
	void work();
	void display();
	void info();
};

#endif // TFRED_H
