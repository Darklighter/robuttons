#include "Tfred.h"
#include "SimulationWidget.h"

void Tfred::work() {
	works=true;
	/*
	if(kill) {
	timer->disconnect(this, SLOT(work()));
	delete timer;
	works=false;
	return;
	}*/
	if(data->simTime!=timer->interval())
		timer->setInterval(data->simTime);
	if(sectorFlag) {
		data->eraseSectors();
		data->setSectorChoice(sectorChoice);
		data->computeSectors();
		sectorFlag = false;
	}
	if(data->getSectorChoice()==0)
		data->moveBruteForce();
	else if(data->getSectorChoice()==1)
		data->moveSector_local();
	else if(data->getSectorChoice()==2)
		data->moveSector_global();
	data->times[0]++;
	works=false;
}
void Tfred::display() {
	works=true;
	if(data->displayTime!=timer->interval())
		timer->setInterval(data->displayTime);
	if(initNew) {
		glInit();
	}
	if(resize) {
		glResize();
	}
	if(!data->shdw)// std
		data->draw();
	else
		data->drawTest();
	works=false;
}
void Tfred::info() {
	works=true;
	/*
	qDebug(QVariant(data->times[2]).toByteArray() + " ms draw, "
		+ QVariant(data->times[1]).toByteArray() + " FPS | "
		+ QVariant(data->times[6]).toByteArray() + " ms sectorize | "
		+ QVariant(data->times[7]).toByteArray() + " ms collisions ("
		+ QVariant(data->times[3]).toByteArray() + ") | "
		+ QVariant(data->times[0]).toByteArray() + "  CPS");
	*/
	data->statusMessage(QVariant(data->times[0]).toString() + "  CPS | "
		+ QVariant(data->times[6]).toString() + " ms sectorize, "
		+ QVariant(data->times[7]).toString() + " ms collisions("
		+ QVariant(data->times[3]).toString() + ", "
		+ QVariant(data->times[4]).toString() + ", "
		+ QVariant(data->times[5]).toString() + ") | "
		+ QVariant(data->times[8]).toString() + "/"
		+ QVariant(data->times[2]).toString() + " ms draw, "
		+ QVariant(data->times[1]).toString() + " FPS"
		+ " | OGL " + QVariant(data->format().majorVersion()).toString()
		+ "." + QVariant(data->format().minorVersion()).toString());
	data->times[0] = data->times[1] = 0; //reset CPS & FPS
	works=false;
}
void Tfred::pause() {
	timer->blockSignals(true);
	paused=true;
	qDebug(QVariant((int)id).toByteArray() + ": paused");
}
void Tfred::resume() {
	timer->blockSignals(false);
	paused = false;
	qDebug(QVariant((int)id).toByteArray() + ": resumed");
}
bool Tfred::pauses() {
	return paused;
}
bool Tfred::working() {
	return works;
}
void Tfred::immediateUpdate() {
	if(paused) timer->blockSignals(false);
	timer->singleShot(0, this, 0);
	while(!works) Sleep(1);
	if(paused) timer->blockSignals(true);
}
void Tfred::initGL() {
	initNew=true;
	changeViewport();
}
void Tfred::glInit() 
{
	data->makeCurrent();// grab Context first
	glMatrixMode(GL_PROJECTION);// Darstellungstransformation
	// Berechnung der Rückseite abstellen
	glEnable(GL_CULL_FACE);//glCullFace(GL_BACK);
	glPolygonMode(GL_FRONT, GL_FILL);

	//glEnable(GL_MULTISAMPLE_ARB);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_POINT_SMOOTH);// rendert große Punkte als Kreise anstatt als Quadrate
	glEnable(GL_LINE_SMOOTH);// Antialiasing für Linien einschalten
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_BLEND);

	//glBlendFunc(GL_SRC_ALPHA_SATURATE, GL_ONE); //Polygon AA
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //Punkt AA
	glMatrixMode(GL_MODELVIEW);// Geometrietransformationsmodus
	initNew=false;
}
void Tfred::changeViewport() {
	resize=true;
	immediateUpdate();
}
void Tfred::glResize() {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, data->width(), data->height());
	glOrtho(-data->movex/data->scale, data->tableWidth+data->movex/data->scale, data->tableHeight+data->movey/data->scale, -data->movey/data->scale, -1.0, 1.0); // Orthogonalprojektion
	glMatrixMode(GL_MODELVIEW);// Geometrietransformationsmodus
	glLoadIdentity();

	glNewList(1, GL_COMPILE); //normal circle
		data->circlePolygon();
	glEndList();
	glNewList(2, GL_COMPILE); //circle minus outline
		data->circlePolygon(0,0,0,1-1/data->scale);
	glEndList();

	resize=false;
}
void Tfred::computeSectors(int flag) {
	sectorFlag = true;
	sectorChoice = flag;
}
Tfred::Tfred(SimulationWidget *data, int id) {
	this->id = id;
	this->data = data;
	works = kill = sectorFlag = resize = initNew = false;
	if(id==WorkHints::calcer) this->setObjectName("Calcer");
	if(id==WorkHints::displayer) this->setObjectName("Displayer");
	if(id==WorkHints::informer) this->setObjectName("Informer");
	this->start();
}
void Tfred::run() {
	works = kill = false;
	timer = new QTimer();
	if(id == WorkHints::calcer) {
		connect(timer, SIGNAL(timeout()), this, SLOT(work()), Qt::DirectConnection);
		timer->setInterval(data->simTime);
	}
	if(id == WorkHints::displayer) {
		connect(timer, SIGNAL(timeout()), this, SLOT(display()), Qt::DirectConnection);
		timer->setInterval(data->displayTime);
		glInit();
		resize=true;
		/*
		glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
		glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
		*/
		/*
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, MyTex);
		glAlphaFunc(GL_GREATER, 0.1); // nur bei alpha>0.1 rendern
		glBegin(GL_QUADS);
		glTexCoord2f(0,0); glVertex3f(-Breite/2, -Höhe/2, -Tiefe);
		glTexCoord2f(1,0); glVertex3f(+Breite/2, -Höhe/2, -Tiefe);
		glTexCoord2f(1,1); glVertex3f(+Breite/2, +Höhe/2, -Tiefe);
		glTexCoord2f(0,1); glVertex3f(-Breite/2, +Höhe/2, -Tiefe);
		glEnd;
		//glDisable(GL_DEPTH_TEST); // disables z-buffer(not used in 2d-rendering)
		//glDepthFunc(GL_LESS oder GL_EQUAL)
		glTranslatef(320, 240, 0);
		glRotatef(45, 0,0,1);
		DrawQuad(0,0,0, 256,256);
		glScalef(2,2,1);
		DrawQuad(0,0,0, 256,256);
		glBindTexture(GL_TEXTURE_2D, MapTex);
		glTranslatef(MapPos.x, MapPos.y, 0);
		glScalef(MapScale, MapScale, MapScale);
		DrawQuad(0,0,0, MapSize.x,MapSize.y);
		*/
	}
	if(id == WorkHints::informer) {
		connect(timer, SIGNAL(timeout()), this, SLOT(info()), Qt::DirectConnection);
		timer->setInterval(1000);
	}
	if(id!=WorkHints::displayer) pause();
	timer->start();
	qDebug(QVariant(id).toByteArray() + ": init");
	exec(); //start event loop, timers work now
	qDebug(QVariant(id).toByteArray() + ": ended");
}
/*
void Tfred::kills() {
kill = true;
resume();
while(works)
Sleep(1);
}
*/
