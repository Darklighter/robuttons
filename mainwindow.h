#include "SimulationWidget.h"

#include <QCheckBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMainWindow>
#include <QPushButton>
#include <QSpinBox>
#include <QVBoxLayout>
#include <QWidget>

class MainWindow : public QMainWindow {
	Q_OBJECT
	QWidget centralWidget;
public:
	MainWindow();

private slots:
	void startStop();
	void pauseUnpause();
	void resetRobuttonCount();

private:
	void enableControls(bool flag);

	bool isRunning{false};
	bool isPaused{false};

	SimulationWidget simulationWidget{this};

	QVBoxLayout layout;
	QGridLayout controlLayout;
	QHBoxLayout startPauseLayout;

	QLabel tableWidthLabel{"table width: "};
	QLabel tableHeightLabel{"table height: "};
	QLabel robuttonCountLabel{"robutton count: "};
	QLabel coinCountLabel{"coin count: "};
	QLabel stepLabel{"step size: "};
	QLabel timeLabel{"simulation timer (ms): "};
	QLabel syncLabel{"render timer (ms): "};
	QLabel cornerLabel{"corner count: "};

	QSpinBox tableWidthSpinBox;
	QSpinBox tableHeightSpinBox;
	QSpinBox robuttonCountSpinBox;
	QSpinBox coinCountSpinBox;
	QDoubleSpinBox stepSpinBox;
	QSpinBox timeSpinBox;
	QSpinBox syncSpinBox;
	QSpinBox cornerSpinBox;

	QPushButton startStopButton{"Start simulation"};
	QPushButton pauseButton{"Pause"};

	QCheckBox check1{"collision"};
	QComboBox comboSector;
	QComboBox combo1;
	QCheckBox check2{"shadow"};
	QCheckBox check3{"outline"};
};
